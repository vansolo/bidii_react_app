(function ($) {
   "use strict";
   var tpj = jQuery;
   var revapi10;
   tpj(document).ready(function () {
      if (tpj("#rev_slider_10_1").revolution == undefined) {
         revslider_showDoubleJqueryError("#rev_slider_10_1");
      } else {
         revapi10 = tpj("#rev_slider_10_1").show().revolution({
            sliderType: "standard",
            sliderLayout: "fullwidth",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
               keyboardNavigation: "off",
               keyboard_direction: "horizontal",
               mouseScrollNavigation: "off",
               mouseScrollReverse: "default",
               onHoverStop: "off",
               arrows: {
                  style: "hermes",
                  enable: true,
                  hide_onmobile: false,
                  hide_onleave: true,
                  hide_delay: 200,
                  hide_delay_mobile: 1200,
                  tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div>	<div class="tp-arr-titleholder">{{title}}</div>	</div>',
                  left: {
                     h_align: "left",
                     v_align: "center",
                     h_offset: 0,
                     v_offset: 0
                  },
                  right: {
                     h_align: "right",
                     v_align: "center",
                     h_offset: 0,
                     v_offset: 0
                  }
               }
            },
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: 1920,
            gridheight: 970,
            lazyType: "none",
            shadow: 0,
            spinner: "spinner3",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
               simplifyAll: "off",
               nextSlideOnWindowFocus: "off",
               disableFocusListener: false
            }
         });
      }


   });

})(jQuery);