import React from 'react';
import bgg from './../images/bg/building-bg.jpg';


class ScheduleDemo extends React.Component {
    render() {
        return (
            <div>

                <section className="coming-soon page-section-ptb bg-overlay-white-90"
                         style={{backgroundImage: 'url('+bgg+')'}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12 text-left">
                                <a href="index.html" className="bdmax-btn " style={{color:'#000'}}>Home</a>
                            </div>
                            <div className="col-sm-12 mt-30 text-center">
                                <h2 className="title text-uppercase mb-10">
                                    Request a Demo
                                </h2>
                                <div className="text-center">
                                    The best way to understand if BidiiBuild is the right solution<br />
                                    for your construction project management needs<br />
                                    is to speak with our construction experts.
                                </div>
                                <h5>BidiiBuild &copy; 2017 </h5>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="action-box yellow-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 col-sm-12 text-center">
                                <h5 className="pt-10">&nbsp;</h5>
                                <h4>Provide your contact details!</h4>
                            </div>

                            <div className="row">

                                <div className="col-md-12 col-sm-12 text-center">
                                    <form className="notify-form"
                                          method="post"
                                          action="https://bidiibuild.us16.list-manage.com/subscribe/post">

                                        <input type="hidden" name="u" value="98ffdb65c070907199adb6092" />
                                        <input type="hidden" name="id" value="8f587871c2" />

                                        <div>
                                            <input
                                                required
                                                autoComplete="off"
                                                type="text"
                                                style={{width:'350px', margin:'0 0 20px 0'}}
                                                name="MERGE1"
                                                id="MERGE1"
                                                className="form-control"
                                                placeholder="Full Name" />
                                        </div>

                                        <div>
                                            <input
                                                required
                                                autoComplete="off"
                                                type="email"
                                                name="MERGE0"
                                                id="MERGE0"
                                                style={{width:'100%' , margin:'0 0 20px 0'}}
                                                className="form-control"
                                                placeholder="Your email"
                                            />
                                        </div>

                                        <div>
                                            <input
                                                required
                                                autoComplete="off"
                                                type="text"
                                                name="MMERGE3"
                                                id="MMERGE3"
                                                style={{width:'100%' , margin:'0 0 20px 0'}}
                                                className="form-control"
                                                placeholder="Company"
                                            />
                                        </div>

                                        <div>
                                            <input
                                                required
                                                autoComplete="off"
                                                type="text" style={{width:'100%' , margin:'0 0 20px 0'}}
                                                name="MERGE2"
                                                id="MERGE2"
                                                className="form-control"
                                                placeholder="Phone"
                                            />
                                        </div>

                                        <input
                                            type="submit"
                                            value="Request Now"
                                            style={{
                                                margin: '0',
                                                width: '100%'
                                            }}
                                            className="cm-btn button border" />

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default ScheduleDemo;