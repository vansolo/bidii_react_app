import React from 'react';
import c4 from './../images/bidiibuild.png';

let Footer = () => {
  return(
     <div>
        {/* Start Footer */}
        <footer className="footer dark-bg page-section-pt pb-0" id="contact">
           <div className="container">
              <div>
                 <div className="col-lg-3 col-md-3 col-sm-6 mb-30">
                    <div className="footer-about">
                       <div className="section-title">
                          <a href="/" className="home-scroll">
                             <img src={c4}
                                  alt="bidiibuild"
                                  width={200}
                                  height={54}
                                  className="wow animated bounceInLeft"
                                  data-wow-duration="1.98s"
                             />
                          </a>
                       </div>
                       <span className="mb-20 btn-block">

                       </span>
                       <p  data-wow-delay="0.2s"
                           data-wow-duration="1.98s"
                           className="wow animated bounceInLeft">
                           Complete your project within time and budget. Use BidiiBuild to track
                          your project's progress, budget and inventory as well as enhancing effective
                          communication and collaboration among team members
                          </p>
                    </div>
                 </div>
                 <div className="col-lg-3 col-md-3 col-sm-6 mb-30">
                    <div className="footer-link">
                       <div className="section-title"><h4 className="title">Our Support</h4></div>
                       <ul className="list-mark">
                          <li><a href="#index" className="home-scroll">Home</a></li>
                          <li><a href="#company" className="page-scroll">Company</a></li>
                          <li><a href="#features" className="page-scroll">Features</a></li>
                          <li><a href="#contact" className="page-scroll">Contact</a></li>

                       </ul>
                    </div>
                 </div>
                 <div className="col-lg-3 col-md-3 col-sm-6 mb-30">
                    <div className="footer-newsletter">
                       <div className="section-title"><h4 className="title">Newsletter</h4></div>
                       <div className="input-group divcenter">
                          <input type="email" placeholder="Enter your Email" className="form-control" />
                          <span className="input-group-btn">
                              <button type="submit" className="btn btn-icon"><i className="fa fa-paper-plane" /></button>
                           </span>
                       </div>
                    </div>
                    <div className="footer-social mt-40 text-white">
                       <div className="section-title"><h4 className="title mb-20">Stay Connected</h4></div>
                       <div className="social-icons color-hover">
                          <ul>
                             <li className="social-facebook wow animated fadeInUp"
                                 data-wow-duration="1.8s">
                                <a href="javascript:void(0)"><i className="fa fa-facebook" /></a>
                             </li>
                             <li className="social-twitter wow animated fadeInUp"
                                 data-wow-duration="1.8s"
                                 data-wow-delay="0.3s">
                                 <a href="javascript:void(0)"><i className="fa fa-twitter" /></a>
                             </li>
                             <li className="social-linkedin wow animated fadeInUp"
                                 data-wow-duration="1.8s"
                                 data-wow-delay="0.5s">
                                 <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/11200528/">
                                    <i className="fa fa-linkedin" />
                                 </a>
                             </li>
                          </ul>
                       </div>
                    </div>
                 </div>
                 <div className="col-lg-3 col-md-3 col-sm-6 mb-30">
                    <div className="footer-address">
                       <div className="section-title">
                           <h4 data-wow-duration="1.98s"
                               className="title wow animated bounceInRight">Get in Touch</h4>
                       </div>
                       <ul data-wow-delay="0.7s"
                           data-wow-duration="1.98s"
                           className="list-inline wow animated bounceInRight">
                          <li>
                              <i className="fa fa-map-marker" />
                              <span>20 Aluguntugu Street, Ambassadorial Enclave, East Legon, Accra</span>
                          </li>
                          <li><i className="fa fa-phone" /> <span>+233 54 969 1947</span></li>
                          <li><i className="fa fa-phone" /> <span>+254 71 943 9168</span></li>
                          <li><i className="fa fa-envelope" /> <span>support@bidiibuild.com</span></li>
                       </ul>
                    </div>
                 </div>
              </div>
           </div>
           <div className="footer-widget mt-50">
              <div className="container"><div className="row">
                 <div className="col-lg-6 col-md-6 col-sm-6">
                    <p data-wow-delay="0.5s"
                       data-wow-duration="1.98s"
                       className="text-white wow animated fadeInUp"> ©Copyright <span id="copyright"> </span>
                       <a href="javascript:void(0)"> BidiiBuild</a> All Rights Reserved
                    </p>
                 </div>
                 <div className="col-lg-6 col-md-6 col-sm-6">
                    <ul data-wow-delay="0.6s"
                        data-wow-duration="1.98s"
                        className="text-right wow animated fadeInUp">
                       <li><a href="javascript:void(0)">Privacy Policy</a></li>
                       <li><a href="javascript:void(0)">Contact Us</a></li>
                    </ul>
                 </div>
              </div>
              </div>
           </div>
        </footer>

        {/* Back to top */}
        <div id="back-to-top">
           <a className="top arrow" href="#top">
              <i className="fa fa-chevron-up" />
           </a>
        </div>
     </div>
  )
};

export default Footer;