import React from 'react';
import { Link } from 'react-router-dom';
import c4 from './../images/bidiibuild.png';

let Nav = () => {
  return (
     <div className="menu" style={{backgroundColor:'#7f8c8d'}}>
        {/* menu start */}
        <nav id="menu" className="mega-menu">
           <section className="menu-list-items">
              <div className="container">
                 <div className="row">
                    <div className="col-lg-12 col-md-12">
                       <ul className="menu-logo">
                          <li>
                             <Link to="/">
                                 <img src={c4}
                                      alt="bidiibuild"
                                      width={200}
                                      height={54}
                                 />
                             </Link>
                          </li>
                       </ul>
                       <ul className="menu-links">
                          <li>
                             <Link to="/" className="home-scroll">Home</Link>
                          </li>
                          <li>
                             <a href="#features" className="page-scroll">Features</a>
                          </li>

                           {/*<li>*/}
                               {/*<a href="#pricing" className="page-scroll">Pricing</a>*/}
                           {/*</li>*/}

                          <li>
                              <a href="#company" className="page-scroll">Company</a>
                          </li>

                          <li>
                              <a href="#contact" className="page-scroll">Contact</a>
                          </li>
                          <li><a>
                              <span>
                                 <a href="https://app.bidiibuild.com" className="link">
                                    Login
                                 </a>
                              </span>
                          </a>
                          </li>
                          <li><a>
                            <span>
                                 <a href="/schedule-demo" className="link">
                                    REQUEST A DEMO
                                 </a>
                              </span>
                          </a>
                          </li>
                       </ul>
                    </div>
                 </div>
              </div>
           </section>
        </nav>
        {/* menu end start */}
     </div>
  )
};

export default Nav;