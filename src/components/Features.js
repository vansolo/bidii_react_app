import React from 'react';

import task          from '../images/flaticons/task.png';
import book          from '../images/flaticons/book.png';
import wifi          from '../images/flaticons/wifi.png';
import calender      from '../images/flaticons/calender.png';
import time          from '../images/flaticons/time.png';
import collaborator  from '../images/flaticons/collaborate.png';


let Features = () => {
   return(
      <section id="features" className="page-section-ptb pb-30 fixed pattern-overlay pattern-4">
         <div className="row row-company">
            <div className="col-sm-12">
                <div className="section-title text-center">
                   <h2 className="title wow slideInRight animated">BidiiBuild Features</h2>
                   <p>
                       Construction Project Management (CPM) is the overall planning,
                      coordination, and control of a project from beginning to completion.
                      CPM is aimed at meeting a client's requirement in order to produce
                      a functionally and financially viable project.
                      BidiiBuild is carefull optimised with features to facilitate this process.
                   </p>
                </div>
            </div>
            <div className="col-sm-12">
               <div className="feature-box line">
                  <img src={task}
                       alt="task"
                       className="wow animated fadeInUp"
                       data-wow-duration="2s"
                  />
                  <h3 className="title">Task Manager</h3>
                  <p>Easy and simple task management tool for easy task monitoring</p>
               </div>
               <div className="feature-box line">
                  <img src={collaborator}
                       alt="collaboration"
                       className="wow animated fadeInUp"
                       data-wow-delay="0.2s"
                       data-wow-duration="2s"
                  />
                  <h3 className="title">Collaborator</h3>
                  <p>BidiiBuild's collaborative functionality adds extra gear to your project.</p>
               </div>
               <div className="feature-box line">
                  <img src={book}
                       alt="inventory tracker"
                       className="wow animated fadeInUp"
                       data-wow-delay="0.3s"
                       data-wow-duration="2s"
                  />
                  <h3 className="title">Inventory tracker</h3>
                  <p>Know who takes what material at what time and for what purpose.</p>
               </div>
               <div className="feature-box line">
                   <div className="wow animated fadeInUp"
                       data-wow-delay="0.5s"
                       data-wow-duration="2s">
                       <img src={wifi} alt="online and offline" />
                       <i className="danger">!</i>
                   </div>
                  <h3 className="title">Online/Offline capability</h3>
                  <p>BidiiBuild works both online and offline. </p>
               </div>
               <div className="feature-box line">
                  <img src={calender}
                       alt="budget tracker"
                       className="wow animated fadeInUp"
                       data-wow-delay="0.6s"
                       data-wow-duration="2s"
                  />
                  <h3 className="title">Budget tracker</h3>
                  <p>BidiiBuild monitors your project with an eagle eye in order to ensure every
                     single cost is properly accounted for.
                  </p>
               </div>
               <div className="feature-box line">
                  <img src={time}
                       alt="Reminder"
                       className="wow animated fadeInUp"
                       data-wow-delay="0.8s"
                       data-wow-duration="2s"
                  />
                  <h3 className="title">Reminder</h3>
                  <p>BidiiBuild reminds employees of assigned tasks  as well as
                     their daily logs in order to keep them on focus.
                  </p>
               </div>
            </div>
         </div>
      </section>
   )
};

export default Features;