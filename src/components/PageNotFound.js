import React from 'react';
import img404 from './../images/error-404.png';
import bgg from './../images/bg/building-bg.jpg';

const PageNotFound = () => {
    return(
        <section className="error-404 page-section-ptb bg-overlay-white-90"
                 style={{backgroundImage: 'url('+bgg+')'}}>
            <div className="container">
                <div className="row">
                    <div className="col-sm-12 text-center">
                        <img style={{display: 'inline'}}
                             className="img-responsive"
                             src={img404} alt="404" />
                    </div>
                    <div className="col-sm-8 col-sm-offset-2 mt-30 text-center">
                        <h3 className="title mb-5">BidiiBuild</h3>
                        <h3 className="title mb-10">The Page you were looking for, couldn't be found</h3>
                        <p>The page you are looking for might have been removed, had its name changed,
                            <br /> or is temporarily unavailable temporarily unavailable
                        </p>
                        <a className="button border animated full-rounded right-icn" href="/">
                            <span>
                                Go to Home Page<i className="fa fa-home" aria-hidden="true" />
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    )
};

export default  PageNotFound;
