import React from 'react';


let Princing = () => {
    return(

        <section className="pricing-section" id="pricing">
            <div className="section-title text-center">
                <h2 className="title wow slideInRight animated">
                    Our Packages
                </h2>
            </div>


            <div className="row price-row ">
                <div className="col-md-3 pricebox pbox1">
                    <dt className="price-package-name">Free Build</dt>
                    <span className="dollar-sign">$</span>
                    <span className="price-figure">0</span>
                    <span className="cent">00</span>

                    <div className="package-details">
                        <h6 className="description">
                            Description
                        </h6>

                        <ul>
                            <span>Max. 2 projects</span><br/>
                            <span>Max. 10 Users</span><br/>
                            <span>Access to all features</span>
                        </ul>
                    </div>
                </div>
                <div className="col-md-3 pricebox pbox2">
                    <dt className="price-package-name">Basic Build</dt>
                    <span className="dollar-sign">$</span>
                    <span className="price-figure">99</span>
                    <span className="cent">99</span>

                    <div className="package-details">
                        <h6 className="description" >Description</h6>
                        <ul>
                            <span>Max. 5 projects</span><br/>
                            <span>Max. 15 Users</span><br/>
                            <span>Access to all features</span>
                        </ul>
                    </div>
                </div>
                <div className="col-md-3 pricebox pbox3">
                    <dt className="price-package-name">Medium Build</dt>
                    <span className="dollar-sign">$</span>
                    <span className="price-figure">170</span>
                    <span className="cent">99</span>

                    <div className="package-details">
                        <h6 className="description">Description</h6>
                        <ul>
                            <span>Max. 15 projects</span><br/>
                            <span>Max. 20 Users</span><br/>
                            <span>Access to all features</span>
                        </ul>
                    </div>
                </div>
                <div className="col-md-3 pricebox pbox4">
                    <dt className="price-package-name">Standard Build</dt>
                    <span className="dollar-sign">$</span>
                    <span className="price-figure">299</span>
                    <span className="cent">99</span>

                    <div className="package-details">
                        <h6 className="description">Description</h6>
                        <ul>
                            <span>Max. 20 projects</span><br/>
                            <span>Max. 50 Users</span><br/>
                            <span>Access to all features</span>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    )
};

export default Princing;