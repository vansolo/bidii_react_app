import React from 'react';
import Bg3   from '../images/bg/aboutus.jpg';

let WhoWeAre = () => {
   return(
      <section className="dark-bg page-section-ptb pb-40 dark-bg bg fixed text-white gradient-overlay"
               id="company"
               style={{backgroundImage: 'url('+Bg3+')'}}>
         <div className="container">
            <div >
               <div className="col-md-9">
                  <div className="section-title"><h2 className="title title2"><label>About Us</label></h2></div>
                  <div className="row mt-50">
                     <div className="col-sm-6">
                        <div className="feature-box left_pos small animated wow bounceInLeft"
                             data-wow-duration="1.5s">
                            <i className="glyph-icon flaticon-construction-34" />
                            <h3 className="title">Who we are</h3>
                            <p>BidiiBuild is a simple, smart, efficient and easy to use construction project
                               Management tool carefully designed for the effective management of construction
                               projects.
                            </p>
                        </div>
                     </div>
                     <div className="col-sm-6">
                         <div className="feature-box left_pos small wow animated bounceInRight"
                              data-wow-duration="1.5s"
                              data-wow-delay="0.3s">
                            <i className="glyph-icon flaticon-shield" />
                            <h3 className="title">What we do</h3>
                            <p>We are dedicated to enhancing the entire construction process and experience.
                               By providing a tracking tool, we ensure accountability and transparency which remains
                               a major concern in the construction industry.
                            </p>
                        </div>
                     </div>
                     <div className="col-sm-6">
                         <div className="feature-box left_pos small wow animated bounceInLeft"
                              data-wow-duration="1.5s"
                              data-wow-delay="0.5s">
                             <i className="glyph-icon flaticon-construction-26" />
                            <h3 className="title">Our vision</h3>
                            <p>At BidiiBuild, we envision a future in which both contractors and their clients will
                           forever enjoy a healthy relationship as a result of transparent processes,
                           reduced cost, as well as completion of project within time and budget </p>
                        </div>
                     </div>
                     <div className="col-sm-6">
                        <div className="feature-box left_pos small wow animated bounceInRight"
                             data-wow-duration="1.5s"
                             data-wow-delay="0.7s">
                            <i className="glyph-icon flaticon-projection-screen-with-bar-chart" />
                            <h3 className="title">Our Value</h3>
                            <p>At BidiiBuild, we value Transparency and Accountability, Work and Happiness,
                               and above all Time and Money.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   )
};

export default WhoWeAre;