import React from 'react';

let TopBar = () => {
   return (
      <div className="topbar">
         <div className="container">
            <div className="row">
               <div className="col-lg-6 col-md-6 col-sm-6">
                  <div className="topbar-left text-left">
                     <ul className="list-inline">
                        <li> <i className="fa fa-envelope-o"> </i> support@bidiibuild.com</li>
                        <li> <i className="fa fa-clock-o" /> Mon - Sat 8:00 - 18:00</li>
                     </ul>
                  </div>
               </div>
               <div className="col-lg-6 col-md-6 col-sm-6">
                  <div className="topbar-right text-right">
                     <ul className="list-inline">
                        <li> <i className="fa fa-phone" />  (+233) 265238085 | ( +233) 54969 1947  </li>
                        <li><a href="javascript:void(0)"><i className="fa fa-facebook" /></a></li>
                        <li><a href="javascript:void(0)"><i className="fa fa-twitter" /></a></li>
                        <li>
                           <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/11200528/">
                              <i className="fa fa-linkedin" />
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   )
};

export default TopBar;