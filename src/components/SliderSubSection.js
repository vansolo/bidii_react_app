import React from 'react';
import {Link} from 'react-router-dom';

let SliderSubSection = () => {
  return(
     <section className="action-box small yellow-bg">
        <div className="container">
           <div className="row">
              <div className="col-sm-8 text-left">
                  <h4 className="wow animated fadeInLeft"
                      data-wow-duration="2s">BIDIIBUILD Construction Project Management Software</h4>
              </div>
              <div className="col-sm-4 text-right text-xs-left">
                  <Link
                      to="/schedule-demo"
                      data-wow-delay="1s"
                      data-wow-duration="2s"
                      className="button border animated middle-fill wow fadeInRight"
                  >
                      <span>Get a demo</span>
                  </Link>
              </div>
            </div>
        </div>
     </section>
  )
};

export default SliderSubSection;