import React from 'react';
import TopBar from './TopBar';
import Nav from './Nav';
import RevolutionSlider from './RevolutionSlider';
import SliderSubSection from './SliderSubSection';
import Features from './Features';
import WhoWeAre from './WhoWeAre';
import WhatWeOffer from './WhatWeOffer';
// import Pricing from './Pricing';
import Footer from './Footer';
import Screens from './Screens'
import Bg3 from './../images/bg/aboutus.jpg'


const Home = () => {
   return (
      <div>
         <header id="header" className="default">
            <TopBar />
            <Nav />
             <div style={{height:'220px',}}>
                 <section className="dark-bg page-section-ptb pb-40 dark-bg bg fixed text-white gradient-overlay"
                          id="company"
                          style={{backgroundImage: 'url('+Bg3+')', height:'300px'}}>
                     <div className="container text-center">
                         <div>
                             <h3 style={{fontFamily:'proxima_nova_rgbold', color:'#e67e22'}}>
                                 The Ideal Tool For Project Time, Cost & Resources Management
                             </h3>
                         </div>
                         <large style={{fontFamily:'proxima_nova_rgbold', fontSize:'20px'}}>
                             With BidiiBuild, you get to know where and how your resources are being used on daily basis,
                             effectively manage task, labor, material and equipment and their associated cost.
                         </large>
                         <br/>
                         <br/>
                         <div>
                          <span>
                                 <a href="/schedule-demo" className="link">
                                    REQUEST A DEMO
                                 </a>
                              </span>
                         </div>
                     </div>

                 </section>
             </div>
         </header>

          {/*<SliderSubSection />*/}

          <Features />

          {/*<Pricing/>*/}

         <WhoWeAre />

         <WhatWeOffer />

         <Screens />

         <Footer />

      </div>
   )
};


export default Home;