import React from 'react';
import { Link } from 'react-router-dom';

import './../vendor/theme/css/custom.css'

import Slide1Thumb   from '../images/slider/thumbs/100x50_d8785-videos.jpg';
import Slide1        from '../images/slider/slide2/d8785-videos.jpg';
import Slide2        from '../images/slider/slide2/2807e-slide6-2.jpg';
import Slide3        from '../images/slider/slide2/c0756-slide6-3.png';
import Slide4        from '../images/slider/slide2/macscreen1.png';
import Slide5        from '../images/slider/slide2/darker-collaborator.png';
import Slide3Thumb   from '../images/slider/thumbs/twomen.png';
import RevVideo      from '../vendor/revolution/assets/video/video.mp4';
import Slide2Thumb   from '../images/slider/thumbs/100x50_2807e-slide6-2.jpg';

let RevolutionSlider = () => {
   return(
      <div  className=""
           data-alias="construction-slider6"
           data-source="gallery"
           style={{
              margin: '0px auto',
              backgroundColor: 'transparent',
              padding: '0px',
              marginTop: '0px',
              marginBottom: '0px'
           }}>

         {/* START REVOLUTION SLIDER 5.3.0.2.1 fullwidth mode */}
         <div id="rev_slider_10_1_wrapper" className="rev_slider_wrapper fullwidthbanner-container"
              data-alias="construction-slider6"
              data-source="gallery"
              style={{
                 margin: '0px auto',
                 backgroundColor: 'transparent',
                 height:'100px',
                 padding: '0px',
                 marginTop: '10px',
                 marginBottom: '0px'
              }}>

            {/* START REVOLUTION SLIDER 5.3.0.2.1 fullwidth mode */}
            <div id="rev_slider_10_1" className="rev_slider fullwidthabanner" style={{display: 'none'}}
                 data-version="5.3.0.2.1">
               <ul>
                  {/* SLIDE 1  */}
                  <li data-index="rs-29"
                      data-transition="fade"
                      data-slotamount="default"
                      data-hideafterloop="0"
                      data-hideslideonmobile="off"
                      data-easein="default"
                      data-easeout="default"
                      data-masterspeed={300}
                      data-thumb={Slide1Thumb}
                      data-rotate={0}
                      data-saveperformance="off"
                      data-title="Slide"
                      data-param1="" data-param2="" data-param3="" data-param4="" data-param5=""
                      data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                      data-description="">

                     {/* MAIN IMAGE */}
                     <img src={Slide1} data-bgposition="center center" data-bgfit="cover"
                          alt="slider"
                          className="rev-slidebg"
                          data-no-retina />

                     {/* LAYERS */}
                     {/* BACKGROUND VIDEO LAYER */}
                     <div className="rs-background-video-layer bg-overlay-black-50"
                          data-forcerewind="on"
                          data-volume="mute"
                          data-videowidth="100%"
                          data-videoheight="100%"
                          data-videomp4={RevVideo}
                          data-videopreload="auto"
                          data-videoloop="none"
                          data-forcecover={1}
                          data-aspectratio="16:9"
                          data-autoplay="true"
                          data-autoplayonlyfirsttime="false"/>
                     {/* LAYER NR. 1 */}
                     <div className="tp-caption text-yellow tp-resizeme" id="slide-29-layer-6"
                          data-x="center"
                          data-hoffset="20"
                          data-y="center"
                          data-voffset={-60}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":1230,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 5,
                             whiteSpace: 'nowrap',
                             fontSize: '28px',
                             lineHeight: '34px',
                             fontWeight: 900,
                             fontFamily: 'Roboto'
                          }}>

                         Your partner in construction
                     </div>

                     {/* LAYER NR. 2 */}
                     <div className="tp-caption tp-resizeme" id="slide-29-layer-7"
                          data-x="center"
                          data-hoffset="20"
                          data-y="center"
                          data-voffset={0}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":1940,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 6,
                             whiteSpace: "nowrap",
                             fontSize: '73px',
                             lineHeight: '73px',
                             fontWeight: 900,
                             color: "rgba(255, 255, 255, 1.00)",
                             fontFamily: "Roboto"
                          }}>
                          Manage your project, reduce cost
                     </div>

                     {/* LAYER NR. 3 */}
                     <div className="tp-caption tp-resizeme" id="slide-29-layer-8"
                          data-x="center"
                          data-hoffset=""
                          data-y="center"
                          data-voffset={80}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":2650,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['center','center','center','center']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 7,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             fontWeight: 400,
                             color: 'rgba(255, 255, 255, 1.00)',
                             fontFamily: 'Roboto'
                          }}>
                            <span className="slider_subtitle ">
                           Make every action count, manage your processes, prevent wastages, reduce cost,
                            </span>
                        <br />

                     </div>

                     {/* LAYER NR. 4 */}
                     <div className="tp-caption button yellow animated middle-fill" id="slide-29-layer-9"
                          data-x="center"
                          data-hoffset={-95}
                          data-y="center"
                          data-voffset={175}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="button"
                          data-responsive_offset="on"
                          data-frames='[{"delay":3360,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[12,12,12,12]"
                          data-paddingright="[35,35,35,35]"
                          data-paddingbottom="[12,12,12,12]"
                          data-paddingleft="[35,35,35,35]"
                          style={{
                             zIndex: 8,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             outline: 'none',
                             cursor: 'pointer'
                          }}>
                        <span >
                            <Link to="/schedule-demo" style={{color:'#fff'}}>
                                Get a demo
                            </Link>
                        </span>
                     </div>

                     {/* LAYER NR. 5 */}
                     <div className="tp-caption button border transparent animated middle-fill"
                          id="slide-29-layer-10"
                          data-x="center"
                          data-hoffset={120}
                          data-y="center"
                          data-voffset={175}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="button"
                          data-responsive_offset="on"
                          data-frames='[{"delay":3360,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[12,12,12,12]"
                          data-paddingright="[35,35,35,35]"
                          data-paddingbottom="[12,12,12,12]"
                          data-paddingleft="[35,35,35,35]"
                          style={{
                             zIndex: 9,
                             whiteSpace: 'nowrap',
                             fontSize: '19px',
                             lineHeight: '29px',
                             outline: 'none',
                             cursor: 'pointer'
                          }}>
                           <span>
                            <a href="https://app.bidiibuild.com" style={{color:'#fff'}}>
                                Login
                            </a>
                           </span>
                     </div>
                  </li>

                  {/* SLIDE 2  */}
                  <li data-index="rs-30" data-transition="fade" data-slotamount="default" data-hideafterloop={0}
                      data-hideslideonmobile="off"
                      data-easein="default"
                      data-easeout="default"
                      data-masterspeed={300}
                      data-thumb={Slide2Thumb}
                      data-rotate={0}
                      data-saveperformance="off"
                      data-title="Slide" data-param1="" data-param2=""
                      data-param3="" data-param4="" data-param5="" data-param6=""
                      data-param7="" data-param8="" data-param9="" data-param10=""
                      data-description="">

                     {/* MAIN IMAGE */}
                     <img src={Slide2}  data-bgposition="center center"
                          alt="slider"
                          data-bgfit="cover"
                          data-bgrepeat="no-repeat"
                          className="rev-slidebg" data-no-retina />

                     {/* LAYERS */}
                     {/* LAYER NR. 6 */}
                     <div className="tp-caption tp-resizeme" id="slide-30-layer-12"
                          data-x="center"
                          data-hoffset=""
                          data-y="bottom"
                          data-voffset=""
                          data-width="['none','none','none','none']"
                          data-height="['none','none','none','none']"
                          data-type="image"
                          data-responsive_offset="on"
                          data-frames='[{"delay":730,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{zIndex: 5}}>
                        <img src={Slide3} alt="slider" data-ww="auto" data-hh="auto" data-no-retina/>
                     </div>

                     {/* LAYER NR. 7 */}
                     <div className="tp-caption tp-resizeme" id="slide-30-layer-4"
                          data-x={70}
                          data-y="bottom"
                          data-voffset={40}
                          data-width="['none','none','none','none']"
                          data-height="['none','none','none','none']"
                          data-type="image"
                          data-responsive_offset="on"
                          data-frames='[{"delay":1960,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power1.easeIn"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{zIndex: 6}}>
                        <img src={Slide4} alt="slider" id="mixer" data-ww="968px" data-hh="618px" data-no-retina/>
                     </div>

                     {/* LAYER NR. 8 */}
                     <div className="tp-caption text-yellow tp-resizeme" id="slide-30-layer-6"
                          data-x="right"
                          data-hoffset={120}
                          data-y="center"
                          data-voffset={-100}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":3300,"speed":1000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 7,
                             whiteSpace: 'nowrap',
                             fontSize: '28px',
                             lineHeight: '30px',
                             fontWeight: 900,
                             fontFamily: 'Roboto'
                          }}>
                        Track the present, plan the future
                     </div>

                     {/* LAYER NR. 9 */}
                     <div className="tp-caption tp-resizeme" id="slide-30-layer-7"
                          data-x="right"
                          data-hoffset={110}
                          data-y="center"
                          data-voffset={-40}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":4030,"speed":1000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 8,
                             whiteSpace: 'nowrap',
                             fontSize: '73px',
                             lineHeight: '73px',
                             fontWeight: 900,
                             color: 'rgba(255, 255, 255, 1.00)',
                             fontFamily: 'Roboto'
                          }}>
                        Manage Task & Inventory
                     </div>

                     {/* LAYER NR. 10 */}
                     <div className="tp-caption tp-resizeme" id="slide-30-layer-8"
                          data-x="right"
                          data-hoffset={120}
                          data-y="center"
                          data-voffset={30}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":4730,"speed":1000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['right','right','right','right']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 9,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             fontWeight: 400,
                             color: 'rgba(255, 255, 255, 1.00)',
                             fontFamily: 'Roboto'
                          }}>
                           <span className="slider_subtitle">
                               Track task progress, monitor your inventory
                           </span>
                     </div>

                     {/* LAYER NR. 11 */}
                     <div className="tp-caption button yellow animated middle-fill" id="slide-30-layer-9"
                          data-x="right"
                          data-hoffset={120}
                          data-y="center"
                          data-voffset={100}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="button"
                          data-responsive_offset="on"
                          data-frames='[{"delay":5450,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[12,12,12,12]"
                          data-paddingright="[35,35,35,35]"
                          data-paddingbottom="[12,12,12,12]"
                          data-paddingleft="[35,35,35,35]"
                          style={{
                             zIndex: 10,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             outline: 'none',
                             cursor: 'pointer'
                          }}>
                           <span>
                               <Link to="/schedule-demo" style={{color:'#fff'}}>
                                Get a demo
                            </Link>
                           </span>
                     </div>

                     {/* LAYER NR. 12 */}
                     <div className="tp-caption button border transparent animated middle-fill"
                          id="slide-30-layer-10"
                          data-x="right"
                          data-hoffset={360}
                          data-y="center"
                          data-voffset={100}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="button"
                          data-responsive_offset="on"
                          data-frames='[{"delay":5450,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[12,12,12,12]"
                          data-paddingright="[35,35,35,35]"
                          data-paddingbottom="[12,12,12,12]"
                          data-paddingleft="[35,35,35,35]"
                          style={{
                             zIndex: 11,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             outline: 'none',
                             cursor: 'pointer'
                          }}>
                           <span>
                               <a href="https://app.bidiibuild.com" style={{color:'#fff'}}>
                                   Login
                               </a>
                           </span>
                     </div>
                  </li>

                  {/* SLIDE 3  */}
                  <li data-index="rs-45" data-transition="fade" data-slotamount="default" data-hideafterloop={0}
                      data-hideslideonmobile="off"
                      data-easein="default"
                      data-easeout="default"
                      data-masterspeed={300}
                      data-thumb={Slide3Thumb}
                      data-rotate={0}
                      data-saveperformance="off"
                      data-title="Slide" data-param1="" data-param2=""
                      data-param3="" data-param4="" data-param5="" data-param6=""
                      data-param7="" data-param8="" data-param9="" data-param10=""
                      data-description="">

                     {/* MAIN IMAGE */}
                     <img src={Slide5} classID="slider5" data-bgposition="center center" data-bgfit="cover"
                          alt="slider"
                          data-bgrepeat="no-repeat"
                          className="rev-slidebg"
                          data-no-retina />

                     {/* LAYER NR. 13 */}
                     <div className="tp-caption  text-yellow tp-resizeme" id="slide-45-layer-6"
                          data-x="center"
                          data-hoffset="10"
                          data-y="center"
                          data-voffset={-70}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":1230,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 5,
                             whiteSpace: 'nowrap',
                             fontSize: '28px',
                             lineHeight: '34px',
                             fontWeight: 900,
                             fontFamily: 'Roboto'
                          }}>
                         Build a team and
                     </div>

                     {/* LAYER NR. 14 */}
                     <div className="tp-caption tp-resizeme" id="slide-45-layer-7"
                          data-x="center"
                          data-hoffset=""
                          data-y="center"
                          data-voffset={-10}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":1940,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 6,
                             whiteSpace: 'nowrap',
                             fontSize: '73px',
                             lineHeight: '73px',
                             fontWeight: 900,
                             color: 'rgba(255, 255, 255, 1.00)',
                             fontFamily: 'Roboto'
                          }}>
                        Enhance Communication & Collaboration
                     </div>

                     {/* LAYER NR. 15 */}
                     <div className="tp-caption tp-resizeme" id="slide-45-layer-8"
                          data-x="center"
                          data-hoffset=""
                          data-y="center"
                          data-voffset={60}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="text"
                          data-responsive_offset="on"
                          data-frames='[{"delay":2650,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power1.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['center','center','center','center']"
                          data-paddingtop="[0,0,0,0]"
                          data-paddingright="[0,0,0,0]"
                          data-paddingbottom="[0,0,0,0]"
                          data-paddingleft="[0,0,0,0]"
                          style={{
                             zIndex: 7,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             fontWeight: 400,
                             color: 'rgba(255, 255, 255, 1.00)',
                             fontFamily: 'Roboto'
                          }}>
                            <span className="slider_subtitle text2">
                           Enhance team work by ensuring effective communication and collaboration
                            </span>
                     </div>

                     {/* LAYER NR. 16 */}
                     <div className="tp-caption button yellow animated middle-fill" id="slide-45-layer-9"
                          data-x="center"
                          data-hoffset={-100}
                          data-y="center"
                          data-voffset={155}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="button"
                          data-responsive_offset="on"
                          data-frames='[{"delay":3360,"speed":1000,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[12,12,12,12]"
                          data-paddingright="[35,35,35,35]"
                          data-paddingbottom="[12,12,12,12]"
                          data-paddingleft="[35,35,35,35]"
                          style={{
                             zIndex: 8,
                             whiteSpace: 'nowrap',
                             fontSize: '17px',
                             lineHeight: '29px',
                             outline: 'none',
                             cursor: 'pointer'
                          }}>
                           <span className="text1">
                            <Link to="/schedule-demo" style={{color:'#fff'}}>
                                Get a demo
                            </Link>
                           </span>
                     </div>

                     {/* LAYER NR. 17 */}
                     <div className="tp-caption button border transparent animated middle-fill"
                          id="slide-45-layer-10"
                          data-x="center"
                          data-hoffset={120}
                          data-y="center"
                          data-voffset={155}
                          data-width="['auto']"
                          data-height="['auto']"
                          data-type="button"
                          data-responsive_offset="on"
                          data-frames='[{"delay":3360,"speed":1000,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                          data-textalign="['inherit','inherit','inherit','inherit']"
                          data-paddingtop="[12,12,12,12]"
                          data-paddingright="[35,35,35,35]"
                          data-paddingbottom="[12,12,12,12]"
                          data-paddingleft="[35,35,35,35]"
                          style={{
                             zIndex: 9,
                             whiteSpace: 'nowrap',
                             fontSize: '19px',
                             lineHeight: '29px',
                             outline: 'none',
                             cursor: 'pointer'
                          }}>
                           <span className="text1">
                             <a href="https://app.bidiibuild.com" style={{color:'#fff'}}>
                                Login
                             </a>
                           </span>
                     </div>
                  </li>
               </ul>
               <div className="tp-bannertimer tp-bottom" style={{visibility: 'hidden !important'}}/>
            </div>
         </div>
      </div>
   )
};

export default RevolutionSlider;