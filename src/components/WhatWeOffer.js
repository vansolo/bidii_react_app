import React from 'react';
import live          from '../images/flaticons/live.png';
import anchor        from '../images/flaticons/anchor.png';
import cog           from '../images/flaticons/cog.png';
import bulb          from '../images/flaticons/bulb.png';
import calender      from '../images/flaticons/calender.png';
import collaborator  from '../images/flaticons/collaborate.png';
import screens        from '../images/screens/screens.png';

//screens
import './../vendor/theme/css/screens.css';

let WhatWeOffer = () => {
   return(
      <section className="page-section-ptb pb-30 fixed pattern-overlay pattern-4">
         <div className="container">
            <div>
               <div className="col-sm-12 text-center">
                  <div className="section-title">
                     <h2 className="title wow animated fadeInUp"
                         data-wow-duration="1.3s">
                         What we Offer
                     </h2>
                     <p className="wow animated fadeInUp"
                        data-wow-delay="0.8s"
                        data-wow-duration="1.3s">
                         BidiiBuild is cherished and widely used by contractors because it serves their needs.
                        Everyone loves it because of it's efficiency and smartness. Aside, it's collaborative and
                        offline capabilities, it also offers a wide range of benefits such as:</p>
                  </div>
               </div>
               <div>
                  <div className="col-sm-4">
                     <div className="feature-box text fb">
                        <h4 className="title">
                           <img src={cog}
                                alt="simplicity"
                                className="wow animated fadeInUp"
                                data-wow-duration="2s"
                           /> Simplicity  and Efficiency
                        </h4>
                        <p>
                           We make conscious effort to make BidiiBuild as simple as possible. Little or perhaps
                           no training is required.
                        </p>
                     </div>
                  </div>
                  <div className="col-sm-4">
                     <div className="feature-box text fb">
                        <h4 className="title">
                           <img src={bulb}
                                alt="transparency"
                                className="wow animated fadeInUp"
                                data-wow-delay="0.2s"
                                data-wow-duration="2s"
                           />
                           Transparency
                        </h4>
                        <p>
                           Using BidiiBuild enhances transparency and accountability. Every single process,
                           task or cost is clearly accounted for.
                        </p>
                     </div>
                  </div>
                  <div className="col-sm-4">
                     <div className="feature-box text fb">
                        <h4 className="title">
                           <img src={anchor}
                                alt="industry"
                                className="wow animated fadeInUp"
                                data-wow-delay="0.3s"
                                data-wow-duration="2s"
                           />&nbsp;
                           Industry/process specific
                        </h4>
                        <p>There are several Project Management Softwares. However, BidiiBuild is specifically build
                           to suit the construction process
                        </p>
                     </div>
                  </div>
               </div>
               <div>
                  <div className="col-sm-4">
                     <div className="feature-box text fb">
                        <h4 className="title">
                           <img src={live}
                                alt="live"
                                className="wow animated fadeInUp"
                                data-wow-delay="0.5s"
                                data-wow-duration="2s"
                           />&nbsp;&nbsp;
                           Live Project Overview
                        </h4>
                        <p>BidiiBuild provides you with a quick overview of your project.
                           You get to know the overall status of the project at a glance.
                        </p>
                     </div>
                  </div>
                  <div className="col-sm-4">
                     <div className="feature-box text fb">
                        <h4 className="title">
                           <img src={calender}
                                alt="daily-reports"
                                className="wow animated fadeInUp"
                                data-wow-delay="0.6s"
                                data-wow-duration="2s"
                           />&nbsp;
                           Up-to-date daily reports
                        </h4>
                        <p>Get daily reports on progress, know which tasks are behind or ahead of
                           schedule, and make the relevant decision.</p>
                     </div>
                  </div>
                  <div className="col-sm-4">
                     <div className="feature-box text fb">
                        <h4 className="title">
                           <img src={collaborator}
                                alt="collaboration"
                                className="wow animated fadeInUp"
                                data-wow-delay="0.8s"
                                data-wow-duration="2s"
                           />&nbsp;
                           Enhanced Communication
                        </h4>
                        <p>
                           No time wasting on paper works, enhance communication &amp; collaboration among relevant stakeholders
                           through BidiiBuild
                        </p>
                     </div>
                  </div>
               </div>
            </div>
             <br/>

             {/*<section className="screens-section text-center">*/}
                {/*<div className="section-title">*/}
                   {/*<h2 className="title wow animated fadeInUp"*/}
                       {/*data-wow-duration="1.3s">*/}
                      {/*Multi Screen Friendly*/}
                   {/*</h2>*/}
                {/*</div>*/}

                 {/*<div className="col-md-2"></div>*/}

                 {/*<div className="col-md-8 screen-div " id="mmm">*/}
                      {/*<img src={screens} alt="multi-screen-friendly" className="img-responsive screens"/>*/}
                 {/*</div>*/}


                 {/*<div className="col-md-1"></div>*/}

             {/*</section>*/}
         </div>
         <br/><br/>

      </section>

   )
};

export default WhatWeOffer;