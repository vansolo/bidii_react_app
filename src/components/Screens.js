import React from 'react'

// Assets
import screens from './../images/threescreensfinal.png'


class Screens extends React.Component{
    render(){
        return(
            <section className="screens-section">
                <div className="text-center ">

                    <div className="section-title text-center">
                        <h2 className="title wow slideInRight animated">
                            Multi Screen Friendly
                        </h2>
                    </div>

                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6">
                            <img
                                style={{alignSelf:'center'}}
                                className="img-responsive"
                                src={screens}
                            />
                        </div>
                        </div>
                        <div className="col-md-3"></div>

                </div>
            </section>
        )
    }
}

export default Screens