// Landing Page Stylesheet
import 'bootstrap/dist/css/bootstrap.css';
import './vendor/animate/animate.css';
import './vendor/mega-menu/css/mega_menu.css';
import './vendor/fontawesome/css/font-awesome.css';
import './vendor/flaticon/css/flaticon.css';
import './vendor/owl-carousel/owl.carousel.css';
import './vendor/revolution/css/settings.css';
import './vendor/theme/css/general.css';
import './vendor/theme/css/style.css';
import './vendor/theme/css/bidii-skin-darkblue.css';
import './vendor/theme/css/custom.css';


// React
import React from 'react';
import {
   Route,
   Switch
} from 'react-router-dom';

import Home             from './components/Home';
import ScheduleDemo      from './components/ScheduleDemo';
import PageNotFound     from './components/PageNotFound';

// Resources
class App extends React.Component {

   render() {
      return (
         <div>
             <Switch>
               <Route exact path="/" component={Home} />
               <Route path="/index.html" component={Home} />
               <Route path="/schedule-demo" component={ScheduleDemo} />
               <Route component={PageNotFound} />
            </Switch>
         </div>
      )
   };
}

export default App;
